# jsonplaceholder tests 
> API tests from the command line with love to you! 



## Features


- Written in JavaScript using the Mocha test framework
- Hits the Endpoints from the outside world 
- Assertions on the JSON responses
- Quick and efficent
- Easy to write new tests


## Installation

1) Install [node.js](http://nodejs.org/), by either crack open your favourite package manager: typically `apt-get install nodejs` on Debian/Ubuntu Linux, `brew install node` on a Mac
 or directly from the website [http://nodejs.org](http://nodejs.org/)


2) Clone the repository from Bitbucket:
```
$ git clone git@bitbucket.org:therollingtester/json-placeholder-tests.gi
```

3) Install Mocha globally  
```
$ npm install --global mocha
```

4) CD into the repository 
```
$ cd json-placeholder-tests
```

5) Install all the dependencies by:
```
$ npm install
```

6) CD into /tests and run fire up Mocha
```
$ cd test/ && mocha
```


FAQ

1) To set up linting. Please go to https://github.com/airbnb/javascript and configure the config to your liking. The required NPM modules are already defined in package.json