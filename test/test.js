const request = require('request');
const expect = require('chai').expect;
var should = require('should');
const baseUrl = 'https://jsonplaceholder.typicode.com';


describe('Getting user #9 and validate email address', function() {
    it('Getting user #9 and validate email address', function(done) {
        request.get({ url: `${baseUrl}/users/9` },
            function(error, response, body) {
                var bodyObj = JSON.parse(body);
                var validator = require("email-validator");
                expect(bodyObj.id).to.equal(9);
                console.log(bodyObj.address);
                console.log(bodyObj.email);
                validator.validate(bodyObj.email);
                done();
            });
    });
});

describe('Getting specific post for user #9', function() {
    it('Posting a simple message from user #9', function(done) {
        request.get({
                url: `${baseUrl}/users/9/posts`,
            },
            function(error, response, body) {
                var bodyObj = JSON.parse(body);
                expect(body).to.include('"id": 81', '"title": "tempora rem veritatis voluptas quo dolores vero",', '"body": "facere qui nesciunt est voluptatum voluptatem nisi\nsequi eligendi necessitatibus ea at rerum itaque\nharum non ratione velit laboriosam quis consequuntur\nex officiis minima doloremque voluptas ut aut"');
                done();
            });
    });
});

describe('Posting a simple message from user #9', function() {
    it('Posting a simple message from user #9', function(done) {
        var jsonBody = {
            title: 'foo',
            body: 'bar',
            userId: 9
        };
        request.post({
                url: `${baseUrl}/posts`,
                body: jsonBody,
                json: true
            },
            function(error, response, body) {
                expect(response.statusCode).to.equal(201);
                done();
            });
    });
});




